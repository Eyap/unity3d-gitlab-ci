# unity3d ci 

This repo only contains scripts and configuration files.

(Not affiliated with Unity Technologies)

## Documentation

**Visit [game.ci/docs/gitlab/getting-started](https://game.ci/docs/gitlab/getting-started).**

## License

[MIT](LICENSE.md)
