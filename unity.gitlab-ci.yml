image: $IMAGE:$UNITY_VERSION-base-$IMAGE_VERSION

variables:
  UNITY_CI_REPO: https://gitlab.com/Eyap/unity3d-gitlab-ci
  UNITY_ACTIVATION_FILE: ./unity3d.alf
  UNITY_DIR: $CI_PROJECT_DIR # this needs to be an absolute path. Defaults to the root of your tree.

.unityci-copy-bash-scripts:
  script:
    - apk add --no-cache wget
    - mkdir -p ./ci
    - wget -N -P ./ci $UNITY_CI_REPO/-/raw/main/ci/before_script.sh
    - wget -N -P ./ci $UNITY_CI_REPO/-/raw/main/ci/get_activation_file.sh
    - wget -N -P ./ci $UNITY_CI_REPO/-/raw/main/ci/test.sh
    - wget -N -P ./ci $UNITY_CI_REPO/-/raw/main/ci/build.sh
    - mkdir -p $UNITY_DIR/Assets/Scripts/Editor
    - wget -N -P $UNITY_DIR/Assets/Scripts/Editor $UNITY_CI_REPO/-/raw/main/Scripts/Editor/BuildCommand.cs
    - wget -N -P $UNITY_DIR/Assets/Scripts/Editor $UNITY_CI_REPO/-/raw/main/Scripts/Editor/BuildPostProcess.cs

.unityci_before_script:
  before_script:
    - bash ./ci/before_script.sh

.unityci_cache:
  cache:
    - key: $CI_COMMIT_REF_SLUG
      paths:
        - ci/
    - key: "$CI_PROJECT_NAMESPACE-$CI_PROJECT_NAME-$BUILD_TARGET"
      paths:
        - $UNITY_DIR/Library/

.unityci_license:
  rules:
    - if: '$UNITY_LICENSE != null'
      when: always

.unityci_defaults:
  extends:
    - .unityci_cache
    - .unityci_before_script
    - .unityci_license

### .PRE STAGE ###

unityci-pre:
  extends: .unityci-copy-bash-scripts
  stage: .pre
  image: alpine
  cache:
  - key: $CI_COMMIT_REF_SLUG
    paths:
      - ci/

### GET-UNITY_VERSION ###

get-unity-version:
  stage: get-unity-version
  image: alpine
  script:
    - echo UNITY_VERSION=$(cat $UNITY_DIR/ProjectSettings/ProjectVersion.txt | grep "m_EditorVersion:.*" | awk '{ print $2}') >> unity_version.env
  artifacts:
    reports:
      dotenv: unity_version.env


### GET-ACTIVATION-FILE ###

# run this job when you need to request a license
# you may need to follow activation steps from documentation
unityci-get-activation-file:
  stage: get-activation-file
  cache:
    - key: $CI_COMMIT_REF_SLUG
      paths:
        - ci/
  rules:
    - if: '$UNITY_LICENSE == null'
      when: manual
  variables:
    UNITY_ACTIVATION_FILE: Unity_v$UNITY_VERSION.alf
  script:
    - bash ./ci/get_activation_file.sh
  artifacts:
    paths:
      - $UNITY_ACTIVATION_FILE
    expire_in: 10 min # Expiring this as artifacts may contain sensitive data and should not be kept public

### TEST ###

.unityci-test:
  stage: test
  extends: .unityci_defaults
  script:
    - bash ./ci/test.sh
  artifacts:
    when: always
    expire_in: 2 weeks
  coverage: /<Linecoverage>(.*?)</Linecoverage>/

### BUILD ###

.unityci-build:
  stage: build
  extends: .unityci_defaults
  script:
    - bash ./ci/build.sh
  artifacts:
    paths:
      - $UNITY_DIR/Builds/
